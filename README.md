# Kafka to Elasticsearch Transporter

## Run
```
yarn start
```

## Build
```
docker build -t kafka-to-elasticsearch-transporter .
```

## Configuration

| name | default | description |
| ---- | ------- | ----------- |
| KAFKA_BROKERS | localhost:9092 | Kafka brokers, comma separated |
| KAFKA_GROUP_ID | kafka-to-es-transporter | Kafka group id |
| KAFKA_TOPIC | logstash | Kafka topic to read messages |
| ES_HOST | http://localhost:9200 | Elasticsearch HTTP/HTTPS host |
| ES_INDEX | logstash-%{moment(message["@timestamp"]).format("YYYYMMDD")} | Elasticsearch index to send messages to |
