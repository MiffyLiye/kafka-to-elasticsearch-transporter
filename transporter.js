const { Kafka } = require('kafkajs');
const request = require('request-promise');
const moment = require('moment');
const logger = require('./logger');

class Transporter {
    constructor({ KAFKA_BROKERS, KAFKA_GROUP_ID, KAFKA_TOPIC, ES_HOST, ES_INDEX }) {
        this.config = { KAFKA_BROKERS, KAFKA_GROUP_ID, KAFKA_TOPIC, ES_HOST, ES_INDEX };

        const kafka = new Kafka({
            clientId: KAFKA_GROUP_ID,
            brokers: KAFKA_BROKERS.split(',')
        });

        this.consumer = kafka.consumer({ groupId: KAFKA_GROUP_ID })
    }

    _send = async (message) => {
        try {
            const index = eval('`' + this.config.ES_INDEX.replace(/%/g, '$') + '`');
            await request({
                method: 'POST',
                uri: `${this.config.ES_HOST}/${index}/_doc/`,
                body: message,
                json: true
            });
            logger.debug(`Sent message to ${index} reported at ${moment(message.timestamp).toISOString()}`);
        } catch (err) {
            logger.error(err.toString(), message);
        }
    }

    transport = async () => {
        await this.consumer.connect();
        await this.consumer.subscribe({ topic: this.config.KAFKA_TOPIC, fromBeginning: false })
        logger.info('Transporter started');

        await this.consumer.run({
            eachMessage: async ({ topic, partition, message }) => {
                logger.debug({
                    partition,
                    offset: message.offset,
                    value: message.value.toString(),
                });
                await this._send(JSON.parse(message.value.toString()));
            },
        })
    }
}

module.exports = Transporter;
