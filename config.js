const dotenv = require('dotenv');

dotenv.config();

const env = process.env.NODE_ENV || 'production';
const configs = {
    base: {
        env,
        KAFKA_BROKERS: process.env.KAFKA_BROKERS || 'localhost:9092',
        KAFKA_GROUP_ID: process.env.KAFKA_GROUP_ID || 'kafka-to-es-transporter',
        KAFKA_TOPIC: process.env.KAFKA_TOPIC || 'logstash',
        ES_HOST: process.env.ES_HOST || 'http://localhost:9200',
        ES_INDEX: process.env.ES_INDEX || 'logstash-%{moment(message["@timestamp"]).format("YYYYMMDD")}',
        LOG_LEVEL: process.env.LOG_LEVEL || 'info'
    },
    production: {
        LOG_LEVEL: process.env.LOG_LEVEL || 'warn'
    },
    development: {
        ES_INDEX: process.env.ES_INDEX || 'logstash-development-%{moment(message["@timestamp"]).format("YYYYMMDD")}',
        LOG_LEVEL: process.env.LOG_LEVEL || 'debug'
    },
    test: {
        ES_INDEX: process.env.ES_INDEX || 'logstash-test-%{moment(message["@timestamp"]).format("YYYYMMDD")}'
    }
};
const config = Object.assign(configs.base, configs[env]);

module.exports = config;