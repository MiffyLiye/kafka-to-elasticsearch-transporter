const Transporter = require('./transporter');
const config = require('./config');
const logger = require('./logger');

const transporter = new Transporter(config);
transporter.transport().catch(err => {
    logger.error(err.toString());
    process.exit();
});
