const config = require('./config');

const logger = require('pino')({ level: config.LOG_LEVEL });

module.exports = logger;
